<?php
// +----------------------------------------------------------------------
// | ThinkYaf [ think-admin 始于 thinkphp6 高级用法 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2021-now atuni.cn All rights reserved.
// +----------------------------------------------------------------------
namespace think\admin;

use think\app\MultiApp;

class Service extends \think\Service
{
    public function boot()
    {
        $this->app->event->listen('HttpRun', function () {
            //判断多应用
            $type = class_exists(MultiApp::class) ? 'app' : 'global';
            $this->app->middleware->add(Addons::class, $type);
        });
    }
}
